# Helper class(es) for the CLI tool.

# Copyright 2020 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

from io import RawIOBase, SEEK_SET, UnsupportedOperation

class NullIO(RawIOBase):
    def fileno(self):
        raise OSError("No connected file descriptor.")
    def seek(self, offset, whence=SEEK_SET):
        raise UnsupportedOperation("Not implemented.")
    def truncate(self, size=None):
        raise UnsupportedOperation("Not implemented.")
    def readinto(self, b):
        raise UnsupportedOperation("Not implemented.")
    def write(self, b):
        return len(b)
    def writable(self):
        return True

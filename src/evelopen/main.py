# Copyright 2020-2022, 2024 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

import io, secrets, sys, hashlib
from datetime import datetime, timezone

FORMAT_VERSION = 2
MAGIC = b"PPIE"
MIN_SUPPORTED_VERSION = 1
MAX_SUPPORTED_VERSION = 2
IDENTIFICATOR_BYTES = 16 # NONCE in the v2.0.1+ doc
HASH_BYTES = 64

# Version 1 file layout
# | MAGIC | VERSION | TIME | ID | PAYLOAD | HASH |
#   +- 4 bytes        |      |    |         |
#           +- 1 byte |      |    |         |
#                     +- At least 15 bytes (terminated with ASCII Z)
#                            +- 16 bytes    |
#                                 +- Arbitrary length
#                                           +- 64 bytes

# Version 2 file layout, identical to v1, except for no timestamp
# | MAGIC | VERSION | NONCE | PAYLOAD | HASH |
#   +- 4 bytes        |       |         |
#           +- 1 byte |       |         |
#                     +- 16 bytes       |
#                             +- Arbitrary length
#                                       +- 64 bytes

CHUNK_SIZE = 10_000
assert CHUNK_SIZE > (HASH_BYTES * 2)

# Avoid implementing a local read loop everywhere
# XXX unwrap and is_evelopen_stream depends on this being idempotent
def wrap_reader(reader):
    if isinstance(reader, io.BufferedReader):
        return reader
    return io.BufferedReader(reader)

# Avoid implementing a local write loop everywhere
def wrap_writer(writer):
    if isinstance(writer, io.BufferedWriter):
        return writer
    return io.BufferedWriter(writer)

def write_magic(dst, hasher):
    n = dst.write(MAGIC)
    assert n == len(MAGIC), "Writer does not buffer?"
    hasher.update(MAGIC)

def write_format_version(dst, hasher, version=FORMAT_VERSION):
    version_byte = bytes((version, ))
    assert len(version_byte) == 1, "Version marker too long."
    n = dst.write(version_byte)
    assert n == len(version_byte), "Writer does not buffer?"
    hasher.update(version_byte)

def write_timestamp(dst, hasher):
    now = datetime.now(tz=timezone.utc)
    timestamp = now.strftime("%Y%m%d%H%M%SZ").encode("ASCII")
    n = dst.write(timestamp)
    assert n == len(timestamp), "Writer does not buffer?"
    hasher.update(timestamp)
    return now

# NONCE in the v2 layout doc
def write_identificator(dst, hasher):
    identificator = secrets.token_bytes(IDENTIFICATOR_BYTES)
    n = dst.write(identificator)
    assert n == IDENTIFICATOR_BYTES, "Writer does not buffer?"
    hasher.update(identificator)

def copy_streams(src, dst, hasher):
    written = 0
    chunk = src.read(CHUNK_SIZE)
    while len(chunk) > 0:
        n = dst.write(chunk)
        assert n == len(chunk), "Writer does not buffer?"
        hasher.update(chunk)
        written += n
        chunk = src.read(CHUNK_SIZE)
    return written

def read_format_version(in_stream, hasher):
    v = in_stream.read(1)
    version = ord(v)
    is_supported = MIN_SUPPORTED_VERSION <= version <= MAX_SUPPORTED_VERSION
    if is_supported:
        hasher.update(v)
    return is_supported, version

def read_date(in_stream, hasher):
    buffer = []
    c = in_stream.read(1)
    while len(c) != 0:
        x = c[0]
        if ord('0') <= x <= ord('9'):
            buffer.append(chr(x))
        elif c == b'Z':
            break
        else:
            raise ValueError("Unexpected character in date string.")
        c = in_stream.read(1)
    else:
        raise ValueError("Prematurely ended input, no time zone marker found.")
    if len(buffer) < 14:
        raise ValueError("Date field missing or too short.")

    timestring = "".join(buffer)
    second = int(timestring[-2:])
    minute = int(timestring[-4:-2])
    hour = int(timestring[-6:-4])
    day = int(timestring[-8:-6])
    month = int(timestring[-10:-8])
    year = int(timestring[:-10])
    timestamp = datetime(year=year, month=month, day=day, hour=hour,
	    minute=minute, second=second, tzinfo=timezone.utc)
    buffer.append('Z')
    hasher.update(''.join(buffer).encode("ASCII"))
    return timestamp

def read_identificator(src, hasher):
    identificator = src.read(IDENTIFICATOR_BYTES)
    assert len(identificator) == IDENTIFICATOR_BYTES, \
            "Could not read stream ID."
    hasher.update(identificator)
    return identificator

def write_and_hash(hasher, dst, data):
    n = dst.write(data)
    assert n == len(data), "Writer does not buffer?"
    hasher.update(data)
    return n

def copy_preserve_end(src, dst, hasher):
    size = 0
    chunk = src.read(CHUNK_SIZE)
    buffer = b''
    while len(chunk) > 0:
        if len(chunk) >= HASH_BYTES:
            size += write_and_hash(hasher, dst, buffer)
            buffer = chunk
        elif len(buffer) >= HASH_BYTES:
            # This depends on now knowing: len(chunk) < HASH_BYTES
            bufferpivot = HASH_BYTES - len(chunk)
            size += write_and_hash(hasher, dst, buffer[:-bufferpivot])
            buffer = buffer[-bufferpivot:] + chunk
        else:
            buffer += chunk
        chunk = src.read(CHUNK_SIZE)
    if len(buffer) > HASH_BYTES:
        size += write_and_hash(hasher, dst, buffer[:-HASH_BYTES])
        hashpart = buffer[-HASH_BYTES:]
    else:
        hashpart = buffer
    assert len(hashpart) == HASH_BYTES, \
            "Insufficient input or bug in hash preservation."
    return hashpart, size

def release_streams(in_stream, src, out_stream=None, dst=None):
    if not (in_stream is src):
        src.detach()
    if dst != None:
        dst.flush()
    if not (out_stream is dst):
        assert dst != None, "Do not send out_stream without dst."
        dst.detach()

def is_evelopen_stream(in_stream, hasher=None):
    """is_evelopen_stream(in_stream[, hash_object]) -> boolean

    Check whether a stream looks to an evelopen envelope. This may consume an
    arbitrary number of bytes from the stream.
    """
    src = wrap_reader(in_stream)
    try:
        possible_magic = src.read(4)
    finally:
        release_streams(in_stream, src)
    is_magic = MAGIC == possible_magic
    if is_magic and hasher != None:
        hasher.update(possible_magic)
    return is_magic

def wrap(in_stream, out_stream, verbose=False, dstverbose=sys.stderr,
        version=2):
    """Wrap a stream in an envelope to another.

    Write the contents of in_stream to out_stream, wrapping it in an envelope
    with timestamp and checksum. Optionally write some human readable messages
    to dstverbose.
    """
    src = wrap_reader(in_stream)
    dst = wrap_writer(out_stream)
    hasher = hashlib.sha512()
    try:
        write_magic(dst, hasher)
        write_format_version(dst, hasher, version)
        if version == 1:
            timestamp = write_timestamp(dst, hasher)
        write_identificator(dst, hasher)
        packaged = copy_streams(src, dst, hasher)
        dst.write(hasher.digest())
    finally:
        release_streams(in_stream, src, out_stream, dst)
    if verbose:
        dstverbose.write("File format version: " + str(FORMAT_VERSION) + "\n")
        if version == 1:
            dstverbose.write("Timestamp: " + str(timestamp) + "\n")
        dstverbose.write("Wrapped: " + str(packaged) + " bytes\n")

def unwrap(in_stream, out_stream, verbose=False, dstverbose=sys.stderr,
        show_long=True):
    """Unwrap a stream created by this library.

    Returns a boolean which is True when the calculated checksum matches the
    stored one. Throws exceptions willynilly for unexpected input.

    Writes the envelope payload to out_stream, verifying the envelope headers
    and optionally writing some human readable messages to dstverbose.
    """
    src = wrap_reader(in_stream)
    dst = wrap_writer(out_stream)
    hasher = hashlib.sha512()
    try:
        magic = is_evelopen_stream(src, hasher)
        if not magic:
            raise ValueError("Input stream does not look like evelopen data.")
        is_supported, version = read_format_version(src, hasher)
        if not is_supported:
            raise ValueError("Unsupported envelope version: " + str(version))
        if version == 1:
            timestamp = read_date(src, hasher)
        identificator = read_identificator(src, hasher)
        checksum, size = copy_preserve_end(src, dst, hasher)
    finally:
        release_streams(in_stream, src, out_stream, dst)
    digest = hasher.digest()
    if verbose:
        dstverbose.write("File format version: " + str(version) + "\n")
        if version == 1:
            dstverbose.write("Timestamp: " + str(timestamp) + "\n")
        dstverbose.write("Identificator: " + identificator.hex() + "\n")
        dstverbose.write("Wrapped: " + str(size) + " bytes\n")
        stored_checksum = checksum.hex()
        actual_checksum = digest.hex()
        if show_long:
            dstverbose.write("Stored checksum: " + stored_checksum + "\n")
            dstverbose.write("Actual checksum: " + actual_checksum + "\n")
        for i in range(0, len(actual_checksum), 64):
            dstverbose.write("Stored, {:04x}: ".format(i)
                    + stored_checksum[i:i + 64] + "\n")
            dstverbose.write("Actual, {:04x}: ".format(i)
                    + actual_checksum[i:i + 64] + "\n")
    return digest == checksum

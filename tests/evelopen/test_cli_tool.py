import pytest
import src.evelopen
from io import UnsupportedOperation

def test_fileno():
    dev_null = src.evelopen.NullIO()
    with pytest.raises(OSError):
        dev_null.fileno()

def test_seek():
    dev_null = src.evelopen.NullIO()
    with pytest.raises(UnsupportedOperation):
        dev_null.seek(5)

def test_truncate():
    dev_null = src.evelopen.NullIO()
    with pytest.raises(UnsupportedOperation):
        dev_null.truncate()

def test_readinto():
    dev_null = src.evelopen.NullIO()
    with pytest.raises(UnsupportedOperation):
        dev_null.readinto(bytearray(10))

def test_write():
    dev_null = src.evelopen.NullIO()
    data = b'0123456789'
    assert len(data) == dev_null.write(data)

def test_writable():
    dev_null = src.evelopen.NullIO()
    assert dev_null.writable()


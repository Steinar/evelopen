import io, hashlib, random
import pytest
import src.evelopen
import src.evelopen.main

EXPECTED_TIMESTAMP_LENGTH = len(b"YYYYmmddHHMMSSZ")

def test_wrapping():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)

    src.evelopen.wrap(source, destination)

    wrapped = destination.getvalue()
    assert len(wrapped) == len(src.evelopen.main.MAGIC) + 1 \
            + src.evelopen.main.IDENTIFICATOR_BYTES \
            + len(data) \
            + src.evelopen.main.HASH_BYTES
    assert wrapped[:len(src.evelopen.main.MAGIC)] == src.evelopen.main.MAGIC
    assert wrapped[len(src.evelopen.main.MAGIC) + 1
            + src.evelopen.main.IDENTIFICATOR_BYTES
            : -src.evelopen.main.HASH_BYTES] == data
    hasher = hashlib.sha512()
    hasher.update(wrapped[:-src.evelopen.main.HASH_BYTES])
    assert hasher.digest() == wrapped[-src.evelopen.main.HASH_BYTES:]

def test_v1():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)

    src.evelopen.wrap(source, destination, version=1)
    wrapped = destination.getvalue()
    symmetric = io.BytesIO()
    success = src.evelopen.unwrap(io.BytesIO(wrapped), symmetric)

    assert success
    assert data == symmetric.getvalue()
    assert len(wrapped) == len(src.evelopen.main.MAGIC) + 1 \
            + EXPECTED_TIMESTAMP_LENGTH \
            + src.evelopen.main.IDENTIFICATOR_BYTES \
            + len(data) \
            + src.evelopen.main.HASH_BYTES
    assert wrapped[:len(src.evelopen.main.MAGIC)] == src.evelopen.main.MAGIC
    assert wrapped[len(src.evelopen.main.MAGIC) + 1
            + EXPECTED_TIMESTAMP_LENGTH + src.evelopen.main.IDENTIFICATOR_BYTES
            : -src.evelopen.main.HASH_BYTES] == data
    hasher = hashlib.sha512()
    hasher.update(wrapped[:-src.evelopen.main.HASH_BYTES])
    assert hasher.digest() == wrapped[-src.evelopen.main.HASH_BYTES:]

def test_cutoff_date_field():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    src.evelopen.wrap(source, destination, version=1)
    wrapped = destination.getvalue()
    start_of_stream = wrapped[0:len(src.evelopen.main.MAGIC) + 1
            + EXPECTED_TIMESTAMP_LENGTH - 5]

    with pytest.raises(ValueError):
        src.evelopen.unwrap(io.BytesIO(start_of_stream), io.BytesIO())

def test_too_short_date_field():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    src.evelopen.wrap(source, destination, version=1)
    wrapped = destination.getvalue()
    edited_stream = wrapped[0:len(src.evelopen.main.MAGIC) + 1
            + EXPECTED_TIMESTAMP_LENGTH - 5] \
            + wrapped[len(src.evelopen.main.MAGIC) + 1
                    + EXPECTED_TIMESTAMP_LENGTH - 4:]

    with pytest.raises(ValueError):
        src.evelopen.unwrap(io.BytesIO(edited_stream), io.BytesIO())

def test_noise_in_date_field():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    src.evelopen.wrap(source, destination, version=1)
    wrapped = destination.getvalue()
    edited_stream = wrapped[0:len(src.evelopen.main.MAGIC) + 1
            + EXPECTED_TIMESTAMP_LENGTH - 5] \
            + b'-' \
            + wrapped[len(src.evelopen.main.MAGIC) + 1
                    + EXPECTED_TIMESTAMP_LENGTH - 4:]

    with pytest.raises(ValueError):
        src.evelopen.unwrap(io.BytesIO(edited_stream), io.BytesIO())

def test_symmetry():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)

    src.evelopen.wrap(source, destination)
    symmetric = io.BytesIO()
    success = src.evelopen.unwrap(io.BytesIO(destination.getvalue()),
        symmetric)

    assert success
    assert data == symmetric.getvalue()

def test_symmetry_long_stream():
    random.seed(123456789)
    data = random.getrandbits(10_000_000 * 8).to_bytes(10_000_000, "big")
    destination = io.BytesIO()
    source = io.BytesIO(data)

    src.evelopen.wrap(source, destination)
    symmetric = io.BytesIO()
    success = src.evelopen.unwrap(io.BytesIO(destination.getvalue()),
        symmetric)

    assert success
    assert data == symmetric.getvalue()

def test_empty_stream():
    data = b''
    destination = io.BytesIO()
    source = io.BytesIO(data)

    src.evelopen.wrap(source, destination)
    symmetric = io.BytesIO()
    success = src.evelopen.unwrap(io.BytesIO(destination.getvalue()),
        symmetric)

    assert success
    assert data == symmetric.getvalue()

def test_unsupported_version_number():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    very_new_version = 253

    src.evelopen.wrap(source, destination)
    wrapped = destination.getvalue()
    very_new = wrapped[:len(src.evelopen.main.MAGIC)] \
            + bytes((very_new_version,)) \
            + wrapped[len(src.evelopen.main.MAGIC) + 1:]
    with pytest.raises(ValueError):
        src.evelopen.unwrap(io.BytesIO(very_new), io.BytesIO())

def test_detecting_buffered_writer():
    data = b'abc'
    source = io.BytesIO(data)
    checkbuffer = io.BytesIO()
    destination = io.BufferedWriter(checkbuffer)

    src.evelopen.wrap(source, destination)

    assert data == checkbuffer.getvalue()\
            [-3 - src.evelopen.main.HASH_BYTES:-src.evelopen.main.HASH_BYTES]

def test_invalid_magic_number():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    src.evelopen.wrap(source, destination)
    invalid_magic = b'PPOI' + destination.getvalue()[4:]

    with pytest.raises(ValueError):
        src.evelopen.unwrap(io.BytesIO(invalid_magic), io.BytesIO())

def test_not_matching_checksum():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    src.evelopen.wrap(source, destination)
    invalid_checksum = destination.getvalue()[:-1] \
            + bytes(((destination.getvalue()[-1] ^ -1) & 255,))

    success = src.evelopen.unwrap(io.BytesIO(invalid_checksum), io.BytesIO())
    
    assert success == False

def test_verbose_wrapping_v1():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    comments = io.StringIO()

    src.evelopen.wrap(source, destination, verbose=True, dstverbose=comments,
            version=1)

    assert "Timestamp" in comments.getvalue()

def test_verbose_wrapping():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    comments = io.StringIO()

    src.evelopen.wrap(source, destination, verbose=True, dstverbose=comments)

    assert "File format version:" in comments.getvalue()

def test_verbose_unwrapping_v1():
    data = b'abc'
    source = io.BytesIO(data)
    destination = io.BytesIO()
    src.evelopen.wrap(source, destination, version=1)
    comments = io.StringIO()

    success = src.evelopen.unwrap(io.BytesIO(destination.getvalue()),
            io.BytesIO(), verbose=True, dstverbose=comments)

    assert success
    assert "Timestamp" in comments.getvalue()

def test_verbose_unwrapping():
    data = b'abc'
    source = io.BytesIO(data)
    destination = io.BytesIO()
    src.evelopen.wrap(source, destination)
    comments = io.StringIO()

    success = src.evelopen.unwrap(io.BytesIO(destination.getvalue()),
            io.BytesIO(), verbose=True, dstverbose=comments)

    assert success
    assert "Identificator" in comments.getvalue()

def test_do_not_inadvertently_close_parameter_streams_in_unwrap():
    data = b'abc'
    destination = io.BytesIO()
    source = io.BytesIO(data)
    src.evelopen.wrap(source, destination)
    wrapped = destination.getvalue()
    start_of_stream = wrapped[0:len(src.evelopen.main.MAGIC) + 1
            + src.evelopen.main.IDENTIFICATOR_BYTES - 5]
    stay_open_r = io.BytesIO(start_of_stream)
    stay_open_w = io.BytesIO()

    with pytest.raises(AssertionError):
        src.evelopen.unwrap(stay_open_r, stay_open_w)

    assert stay_open_r.closed == False
    assert stay_open_w.closed == False

def test_do_not_inadvertently_close_parameter_streams_in_wrap(monkeypatch):
    data = b'abc'
    stay_open_r = io.BytesIO(data)
    stay_open_w = io.BytesIO()

    def mock_write(dst, hasher):
        raise OSError("write_identificator")
    monkeypatch.setattr(src.evelopen.main, "write_identificator", mock_write)

    with pytest.raises(OSError):
        src.evelopen.wrap(stay_open_r, stay_open_w)

    assert stay_open_r.closed == False
    assert stay_open_w.closed == False

def test_do_not_inadvertently_close_parameter_streams_in_is_evelopen_stream(
        monkeypatch):
    stay_open = io.BytesIO(b'')
    def mock_read(len=-1):
        raise OSError("readinto")
    monkeypatch.setattr(stay_open, "readinto", mock_read)

    with pytest.raises(OSError):
        src.evelopen.is_evelopen_stream(stay_open)

    assert stay_open.closed == False

def create_stream_w_hash_padding(length):
    data = random.randbytes(length + src.evelopen.main.HASH_BYTES)
    destination = io.BytesIO()
    source = io.BytesIO(data)
    return data, destination, source

def test_copy_preserve_end():
    random.seed(123456789)
    for n in range(src.evelopen.main.CHUNK_SIZE - 1,
            src.evelopen.main.CHUNK_SIZE + 1):
        data, destination, source = create_stream_w_hash_padding(n)
        hasher = hashlib.sha512()

        hashpart, copied = src.evelopen.main.copy_preserve_end(source,
                destination, hasher)

        assert n == copied
        assert hasher.digest() == hashlib.sha512(data[:n]).digest()
        assert destination.getvalue() == data[:n]
        assert len(hashpart) == src.evelopen.main.HASH_BYTES
        assert hashpart == data[n:]

class StingyStreamMock:
    def __init__(self, data):
        self.data = data
        self.position = 0

    def read(self, length):
        prev = self.position
        self.position += 1
        return self.data[prev:self.position]

def test_copy_preserve_end_annoying_input_stream():
    random.seed(123456789)
    amount = 100
    data = random.randbytes(amount + src.evelopen.main.HASH_BYTES)
    destination = io.BytesIO()
    source = StingyStreamMock(data)
    hasher = hashlib.sha512()

    hashpart, copied = src.evelopen.main.copy_preserve_end(source,
            destination, hasher)

    assert amount == copied
    assert hasher.digest() == hashlib.sha512(data[:amount]).digest()
    assert destination.getvalue() == data[:amount]
    assert len(hashpart) == src.evelopen.main.HASH_BYTES
    assert hashpart == data[amount:]

def test_symmetry_many_stream_lengths():
    for amount in (src.evelopen.main.HASH_BYTES - 1,
            src.evelopen.main.HASH_BYTES,
            src.evelopen.main.HASH_BYTES + 1,
            src.evelopen.main.CHUNK_SIZE - 1,
            src.evelopen.main.CHUNK_SIZE,
            src.evelopen.main.CHUNK_SIZE + 1,
            src.evelopen.main.CHUNK_SIZE * 2 - 1,
            src.evelopen.main.CHUNK_SIZE * 2,
            src.evelopen.main.CHUNK_SIZE * 2 + 1,
            src.evelopen.main.CHUNK_SIZE * 3 - 1,
            src.evelopen.main.CHUNK_SIZE * 3,
            src.evelopen.main.CHUNK_SIZE * 3 + 1,
            1,
            2):
        random.seed(123456789)
        data = random.randbytes(amount)
        destination = io.BytesIO()
        source = io.BytesIO(data)

        src.evelopen.wrap(source, destination)
        symmetric = io.BytesIO()
        success = src.evelopen.unwrap(io.BytesIO(destination.getvalue()),
            symmetric)

        assert success
        assert data == symmetric.getvalue()

class WeirdReadStreamMock:
    INIT_DATA = (b'a', b'b' * 1000, b'c' * 1000,
            b'd', b'e', b'f', b'g')
    HASH_PAD = b'h' * src.evelopen.main.HASH_BYTES

    def __init__(self):
        self.data = self.INIT_DATA + (self.HASH_PAD,)
        self.position = 0

    def read(self, length):
        current = b''
        if self.position < len(self.data):
            current = self.data[self.position]
        self.position += 1
        return current

def test_copying_w_weird_input_behavior():
    random.seed(123456789)
    destination = io.BytesIO()
    source = WeirdReadStreamMock()
    hasher = hashlib.sha512()

    hashpart, copied = src.evelopen.main.copy_preserve_end(source,
            destination, hasher)

    expected_data = b''.join(WeirdReadStreamMock.INIT_DATA)
    assert len(expected_data) == copied
    assert hasher.digest() == hashlib.sha512(expected_data).digest()
    assert destination.getvalue() == expected_data
    assert len(hashpart) == src.evelopen.main.HASH_BYTES
    assert hashpart == WeirdReadStreamMock.HASH_PAD
